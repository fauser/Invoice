﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakturering
{
    public class InvoiceRow
    {
        public int P_ID { get; set; }
        public int FTG_NR { get; set; }
        public decimal ANST_NR { get; set; }
        public decimal Antal { get; set; }
        public decimal Belopp { get; set; }
        public int LOP_NR_LONEART { get; set; }
        public string LONEART_TXT { get; set; }
        public int LONEART_ID { get; set; }
        public string Utbdag { get; set; }
        public string SOCAVG_TXT { get; set; }
        public decimal SOCAVG_PROCENT { get; set; }
        public string ARTDEC { get; set; }
        public string USOC { get; set; }
        public string SEM_DAGTYP { get; set; }
        public string LONEARTSTYP { get; set; }
        public int RAD_ID { get; set; }
        public string SEM_TILLAGG { get; set; }
    }
}
