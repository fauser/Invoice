﻿namespace Fakturering
{
    partial class formInvoicePayroll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuItemArchive = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemClose = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemTools = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemMapping = new System.Windows.Forms.ToolStripMenuItem();
            this.tbSearchInvoice = new System.Windows.Forms.TextBox();
            this.buSearch = new System.Windows.Forms.Button();
            this.buCreateTempInvoice = new System.Windows.Forms.Button();
            this.buPreview = new System.Windows.Forms.Button();
            this.buLock = new System.Windows.Forms.Button();
            this.buExport = new System.Windows.Forms.Button();
            this.buClose = new System.Windows.Forms.Button();
            this.gbEmployeeIDSelection = new System.Windows.Forms.GroupBox();
            this.tbEmployeeIdTo = new System.Windows.Forms.TextBox();
            this.tbEmployeeIdFrom = new System.Windows.Forms.TextBox();
            this.gbBasicSettings = new System.Windows.Forms.GroupBox();
            this.cbPeriod = new System.Windows.Forms.ComboBox();
            this.cbGroup = new System.Windows.Forms.ComboBox();
            this.udYear = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1.SuspendLayout();
            this.gbEmployeeIDSelection.SuspendLayout();
            this.gbBasicSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udYear)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemArchive,
            this.menuItemTools});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(478, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuItemArchive
            // 
            this.menuItemArchive.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemClose});
            this.menuItemArchive.Name = "menuItemArchive";
            this.menuItemArchive.Size = new System.Drawing.Size(46, 20);
            this.menuItemArchive.Text = "Arkiv";
            // 
            // menuItemClose
            // 
            this.menuItemClose.Name = "menuItemClose";
            this.menuItemClose.Size = new System.Drawing.Size(104, 22);
            this.menuItemClose.Text = "Stäng";
            this.menuItemClose.Click += new System.EventHandler(this.menuItemClose_Click);
            // 
            // menuItemTools
            // 
            this.menuItemTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemSettings,
            this.menuItemMapping});
            this.menuItemTools.Name = "menuItemTools";
            this.menuItemTools.Size = new System.Drawing.Size(59, 20);
            this.menuItemTools.Text = "Verktyg";
            // 
            // menuItemSettings
            // 
            this.menuItemSettings.Name = "menuItemSettings";
            this.menuItemSettings.Size = new System.Drawing.Size(176, 22);
            this.menuItemSettings.Text = "Inställningar";
            this.menuItemSettings.Click += new System.EventHandler(this.menuItemSettings_Click);
            // 
            // menuItemMapping
            // 
            this.menuItemMapping.Name = "menuItemMapping";
            this.menuItemMapping.Size = new System.Drawing.Size(176, 22);
            this.menuItemMapping.Text = "Översättningstabell";
            this.menuItemMapping.Click += new System.EventHandler(this.menuItemConvertion_Click);
            // 
            // tbSearchInvoice
            // 
            this.tbSearchInvoice.Location = new System.Drawing.Point(52, 180);
            this.tbSearchInvoice.Name = "tbSearchInvoice";
            this.tbSearchInvoice.Size = new System.Drawing.Size(100, 20);
            this.tbSearchInvoice.TabIndex = 6;
            // 
            // buSearch
            // 
            this.buSearch.Location = new System.Drawing.Point(158, 180);
            this.buSearch.Name = "buSearch";
            this.buSearch.Size = new System.Drawing.Size(30, 23);
            this.buSearch.TabIndex = 7;
            this.buSearch.Text = "button1";
            this.buSearch.UseVisualStyleBackColor = true;
            this.buSearch.Click += new System.EventHandler(this.buSearch_Click);
            // 
            // buCreateTempInvoice
            // 
            this.buCreateTempInvoice.Location = new System.Drawing.Point(255, 142);
            this.buCreateTempInvoice.Name = "buCreateTempInvoice";
            this.buCreateTempInvoice.Size = new System.Drawing.Size(75, 23);
            this.buCreateTempInvoice.TabIndex = 8;
            this.buCreateTempInvoice.Text = "Skapa prel. faktura";
            this.buCreateTempInvoice.UseVisualStyleBackColor = true;
            this.buCreateTempInvoice.Click += new System.EventHandler(this.buCreateTempInvoice_Click);
            // 
            // buPreview
            // 
            this.buPreview.Location = new System.Drawing.Point(363, 142);
            this.buPreview.Name = "buPreview";
            this.buPreview.Size = new System.Drawing.Size(75, 23);
            this.buPreview.TabIndex = 9;
            this.buPreview.Text = "Granska";
            this.buPreview.UseVisualStyleBackColor = true;
            this.buPreview.Click += new System.EventHandler(this.buPreview_Click);
            // 
            // buLock
            // 
            this.buLock.Location = new System.Drawing.Point(157, 228);
            this.buLock.Name = "buLock";
            this.buLock.Size = new System.Drawing.Size(75, 23);
            this.buLock.TabIndex = 10;
            this.buLock.Text = "Lås faktura";
            this.buLock.UseVisualStyleBackColor = true;
            this.buLock.Click += new System.EventHandler(this.buLock_Click);
            // 
            // buExport
            // 
            this.buExport.Location = new System.Drawing.Point(255, 228);
            this.buExport.Name = "buExport";
            this.buExport.Size = new System.Drawing.Size(75, 23);
            this.buExport.TabIndex = 11;
            this.buExport.Text = "Fil/Export";
            this.buExport.UseVisualStyleBackColor = true;
            this.buExport.Click += new System.EventHandler(this.buExport_Click);
            // 
            // buClose
            // 
            this.buClose.Location = new System.Drawing.Point(363, 228);
            this.buClose.Name = "buClose";
            this.buClose.Size = new System.Drawing.Size(75, 23);
            this.buClose.TabIndex = 12;
            this.buClose.Text = "Stäng";
            this.buClose.UseVisualStyleBackColor = true;
            this.buClose.Click += new System.EventHandler(this.buClose_Click);
            // 
            // gbEmployeeIDSelection
            // 
            this.gbEmployeeIDSelection.Controls.Add(this.tbEmployeeIdTo);
            this.gbEmployeeIDSelection.Controls.Add(this.tbEmployeeIdFrom);
            this.gbEmployeeIDSelection.Location = new System.Drawing.Point(255, 30);
            this.gbEmployeeIDSelection.Name = "gbEmployeeIDSelection";
            this.gbEmployeeIDSelection.Size = new System.Drawing.Size(200, 100);
            this.gbEmployeeIDSelection.TabIndex = 13;
            this.gbEmployeeIDSelection.TabStop = false;
            this.gbEmployeeIDSelection.Text = "Urval ANSTNR";
            // 
            // tbEmployeeIdTo
            // 
            this.tbEmployeeIdTo.Location = new System.Drawing.Point(50, 54);
            this.tbEmployeeIdTo.Name = "tbEmployeeIdTo";
            this.tbEmployeeIdTo.Size = new System.Drawing.Size(100, 20);
            this.tbEmployeeIdTo.TabIndex = 7;
            // 
            // tbEmployeeIdFrom
            // 
            this.tbEmployeeIdFrom.Location = new System.Drawing.Point(50, 26);
            this.tbEmployeeIdFrom.Name = "tbEmployeeIdFrom";
            this.tbEmployeeIdFrom.Size = new System.Drawing.Size(100, 20);
            this.tbEmployeeIdFrom.TabIndex = 6;
            // 
            // gbBasicSettings
            // 
            this.gbBasicSettings.Controls.Add(this.cbPeriod);
            this.gbBasicSettings.Controls.Add(this.cbGroup);
            this.gbBasicSettings.Controls.Add(this.udYear);
            this.gbBasicSettings.Location = new System.Drawing.Point(12, 27);
            this.gbBasicSettings.Name = "gbBasicSettings";
            this.gbBasicSettings.Size = new System.Drawing.Size(220, 138);
            this.gbBasicSettings.TabIndex = 14;
            this.gbBasicSettings.TabStop = false;
            this.gbBasicSettings.Text = "Basurval";
            // 
            // cbPeriod
            // 
            this.cbPeriod.FormattingEnabled = true;
            this.cbPeriod.Location = new System.Drawing.Point(40, 84);
            this.cbPeriod.Name = "cbPeriod";
            this.cbPeriod.Size = new System.Drawing.Size(121, 21);
            this.cbPeriod.TabIndex = 6;
            this.cbPeriod.SelectedIndexChanged += new System.EventHandler(this.cbPeriod_SelectedIndexChanged);
            // 
            // cbGroup
            // 
            this.cbGroup.FormattingEnabled = true;
            this.cbGroup.Location = new System.Drawing.Point(40, 57);
            this.cbGroup.Name = "cbGroup";
            this.cbGroup.Size = new System.Drawing.Size(121, 21);
            this.cbGroup.TabIndex = 5;
            this.cbGroup.SelectedIndexChanged += new System.EventHandler(this.cbGroup_SelectedIndexChanged);
            // 
            // udYear
            // 
            this.udYear.Location = new System.Drawing.Point(40, 30);
            this.udYear.Name = "udYear";
            this.udYear.Size = new System.Drawing.Size(120, 20);
            this.udYear.TabIndex = 4;
            this.udYear.ValueChanged += new System.EventHandler(this.udYear_ValueChanged);
            // 
            // formInvoicePayroll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 275);
            this.Controls.Add(this.gbBasicSettings);
            this.Controls.Add(this.gbEmployeeIDSelection);
            this.Controls.Add(this.buClose);
            this.Controls.Add(this.buExport);
            this.Controls.Add(this.buLock);
            this.Controls.Add(this.buPreview);
            this.Controls.Add(this.buCreateTempInvoice);
            this.Controls.Add(this.buSearch);
            this.Controls.Add(this.tbSearchInvoice);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "formInvoicePayroll";
            this.Text = "Lönefakturering";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.gbEmployeeIDSelection.ResumeLayout(false);
            this.gbEmployeeIDSelection.PerformLayout();
            this.gbBasicSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.udYear)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuItemArchive;
        private System.Windows.Forms.ToolStripMenuItem menuItemClose;
        private System.Windows.Forms.ToolStripMenuItem menuItemTools;
        private System.Windows.Forms.ToolStripMenuItem menuItemSettings;
        private System.Windows.Forms.ToolStripMenuItem menuItemMapping;
        private System.Windows.Forms.TextBox tbSearchInvoice;
        private System.Windows.Forms.Button buSearch;
        private System.Windows.Forms.Button buCreateTempInvoice;
        private System.Windows.Forms.Button buPreview;
        private System.Windows.Forms.Button buLock;
        private System.Windows.Forms.Button buExport;
        private System.Windows.Forms.Button buClose;
        private System.Windows.Forms.GroupBox gbEmployeeIDSelection;
        private System.Windows.Forms.TextBox tbEmployeeIdTo;
        private System.Windows.Forms.TextBox tbEmployeeIdFrom;
        private System.Windows.Forms.GroupBox gbBasicSettings;
        private System.Windows.Forms.ComboBox cbPeriod;
        private System.Windows.Forms.ComboBox cbGroup;
        private System.Windows.Forms.NumericUpDown udYear;
    }
}

