﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakturering
{
    public enum EnumInvoiceRowIndex
    {
        P_ID = 0,
        FTG_NR = 1,
        ANST_NR = 2,
        ANTAL = 3,
        BELOPP = 4,
        LOP_NR_LONEART = 5,
        LONEART_TXT = 6,
        LONEART_ID = 7,
        UTBET_DATUM = 8,
        SOCAVG_TXT = 9,
        SOCAVG_PROCENT = 10,
        ARTDEC = 11,
        USOC = 12,
        SEM_DAGTYP = 13,
        LONEARTSTYP = 14,
        RAD_ID = 15,
        SEM_TILLAGG = 16
    }
}
