﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fakturering
{
    public partial class formMapping : Form
    {
        public formMapping()
        {
            InitializeComponent();

            List<Corps> corps = new List<Corps>();

            using (SqlConnection connection = new SqlConnection(Helper.ConnectionString()))
            {
                connection.Open();

                StringBuilder cmd = new StringBuilder();

                cmd.AppendLine("select DISTINCT(fp.ARBSTALLENR), IsNull(ek.kar_namn,'') KAR_NAMN from BA_FTG_PERS fp");
                cmd.AppendLine("Left Outer Join TB_EGEN_KAR ek on (ek.kar_nr = fp.arbstallenr)");
                cmd.AppendLine("where fp.ARBSTALLENR is not null");
                cmd.AppendLine("ORDER BY fp.ARBSTALLENR");

                SqlCommand command = new SqlCommand(cmd.ToString(), connection);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDataRecord record = reader;
                    Corps c = new Corps()
                    {
                        Nbr = record.GetDecimal(0),
                        Name = record.GetString(1)
                    };

                    corps.Add(c);
                }
            }

            dataGridView1.DataSource = corps;
        }

        private void buSave_Click(object sender, EventArgs e)
        {
            /*
            procedure TFOversattning.buSparaClick(Sender: TObject);
var
  i: Integer;
  qu: TADOQuery;
begin
  qu:=nil;  //2013-01-18  Måste initieras annars ges en warning
  try
    qu:= TADOQuery.Create(nil);
    qu.Connection := m_qu.Connection;
  with qu do begin
    SQL.Add('TRUNCATE TABLE TB_EGEN_KAR');
//    SQL.SaveToFIle('C:/Oversattning.txt');
    ExecSQL;
  end;

  for i:=1 to sgKar.RowCount - 1 do begin
    qu:= nil;
    qu:= TADOQuery.Create(nil);
    qu.Connection := m_qu.Connection;
    with qu do begin
      SQL.Add('INSERT INTO TB_EGEN_KAR VALUES( '+trim(sgKar.Cells[0,i])+','+chr(39)+trim(sgKar.Cells[1,i])+chr(39)+')');
      ExecSQL;
    end;
  end;
 finally
  qu.Free;
  qu:= nil;
 end;
end;
            */
        }

        private void formMapping_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void buClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void SetHeaders()
        {
            /*  procedure TFOversattning.SetHeaders;
begin
with sgKar do begin
Cells[0,0]:= 'KårNr';
Cells[1,0]:= 'Kårnamn';
end;
end;*/

        }
    }
}
