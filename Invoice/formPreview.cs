﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fakturering
{
    public partial class formPreview : Form
    {
        public formPreview(List<string> fil, List<TPersoner> personer)
        {
            InitializeComponent();

            if (fil != null && fil.Count() > 0)
            {
                buSave.Enabled = false;
            }

            SetHeaders();

            dataGridView1.DataSource = personer;



            /*
            constructor TFGranskning.Create(fil: TStringlist);
var
  i,j: Integer;
  Personer: TPersoner;
  //Fran, Till: Integer;  //2013-01-18 Används inte i unit

begin
 inherited Create(nil);
  if fil = nil then
    buSparaAndringar.Enabled:= false;
  
  SetHeaders;
  j:=1;
  for i:=0 to Length(FLonefaktura.vvList) -1 do
  begin
    Personer:= FLonefaktura.vvList[i];
    with sgTransar do begin
      if Personer.P_ID <> 0 then begin
        Cells[0,j]:= IntToStr(Personer.P_ID);
        Cells[1,j]:= IntToStr(Personer.FTG_NR);
        Cells[2,j]:= Personer.Namn;
        Cells[3,j]:= IntToStr(Personer.ANST_NR);
        Cells[4,j]:= IntToStr(Personer.LoneartID);
        Cells[5,j]:= Personer.Benamning;
        Cells[6,j]:= FloatToStr(Personer.Antal);
        Cells[7,j]:= FloatToStr(Personer.Belopp);
        Cells[8,j]:= Personer.Utbdag;
        Cells[9,j]:= Personer.FakturaNr;
        Cells[10,j]:= Personer.Statuskod;
        Cells[11,j]:= IntToStr(Personer.KarNr);
        Cells[12,j]:= Personer.KarNamn;
        Cells[13,j]:= FloatToStr(Personer.Procent);
        Cells[14,j]:= Personer.Kst;
        RowCount:= RowCount + 1;
      end;
    end;
    Inc(j);
  end;
  sgTransar.RowCount:= sgTransar.RowCount - 1;
  fil.Free;
end;


            */
        }

        private void SetHeaders()
        {
            //throw new NotImplementedException();
        }

        private void buClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buSave_Click(object sender, EventArgs e)
        {
            List<TPersoner> listPersoner = new List<TPersoner>();
            foreach (DataGridViewRow dgvr in dataGridView1.Rows)
            {


                TPersoner p = new TPersoner()
                {
                    ANST_NR = (decimal)dgvr.Cells["ANST_NR"].Value,
                    Antal = (decimal)dgvr.Cells["Antal"].Value,
                    Belopp = (decimal)dgvr.Cells["Belopp"].Value,
                    Benamning = ((string)dgvr.Cells["Benamning"].Value).Trim(),
                    FakturaNr = ((string)dgvr.Cells["FakturaNr"].Value).Trim(),
                    FTG_NR = (int)dgvr.Cells["FTG_NR"].Value,
                    KarNamn = ((string)dgvr.Cells["KarNamn"].Value).Trim(),
                    KarNr = (int)dgvr.Cells["KarNr"].Value,
                    Kst = ((string)dgvr.Cells["Kst"].Value).Trim(),
                    LoneartID = (int)dgvr.Cells["LoneartID"].Value,
                    Namn = ((string)dgvr.Cells["Namn"].Value).Trim(),
                    P_ID = (int)dgvr.Cells["P_ID"].Value,
                    Procent = (decimal)dgvr.Cells["Procent"].Value,
                    Statuskod = ((string)dgvr.Cells["Statuskod"].Value).Trim(),
                    Utbdag = ((string)dgvr.Cells["Utbdag"].Value).Trim()
                };
                listPersoner.Add(p);
            }


            StringBuilder sb = new StringBuilder();
            foreach(TPersoner p in listPersoner)
            {
                sb.AppendLine(p.ToString());
            }
        }

        /*
        procedure TFGranskning.SetHeaders;
begin
 with sgTransar do begin
  Cells[1,0]:= 'FTG_NR';
  Cells[2,0]:= 'Namn';
  Cells[3,0]:= 'ANST_NR';
  Cells[4,0]:= 'LöneartID';
  Cells[5,0]:= 'Benamning';
  Cells[6,0]:= 'Antal';
  Cells[7,0]:= 'Belopp';
  Cells[8,0]:= 'Utbdag';
  Cells[9,0]:= 'FakturaNr';
  Cells[10,0]:= 'Statuskod';
  Cells[11,0]:= 'KarNr';
  Cells[12,0]:= 'KarNamn';
  Cells[13,0]:= 'Procent';
  Cells[14,0]:= 'Kst';
 end;

end;
        */
    }
}