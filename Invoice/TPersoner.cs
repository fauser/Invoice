﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fakturering
{
    public class TPersoner
    {
        public TPersoner()
        {

        }

        public TPersoner(InvoiceRow ir, int currentIncoiveNumber)
        {
            FTG_NR = ir.FTG_NR;
            P_ID = ir.P_ID;
            //Namn
            ANST_NR = ir.ANST_NR;
            LoneartID = ir.LONEART_ID;
            //Benamning
            Antal = ir.Antal == 0 ? 1 : ir.Antal;
            Belopp = ir.Belopp;
            Utbdag = ir.Utbdag;
            FakturaNr = currentIncoiveNumber.ToString();
            Statuskod = 0;
            //KarNr
            //Procent
            //Kst
        }

        public int FTG_NR { get; set; }
        public int P_ID { get; set; }
        public string Namn { get; set; }
        public decimal ANST_NR { get; set; }
        public int LoneartID { get; set; }
        public string Benamning { get; set; }
        public decimal Antal { get; set; }
        public decimal Belopp { get; set; }
        public string Utbdag { get; set; }
        public string FakturaNr { get; set; }
        public string Statuskod { get; set; }
        public int KarNr { get; set; }
        public string KarNamn { get; set; }
        public decimal Procent { get; set; }
        public string Kst { get; set; }

        public override string ToString()
        {
            return String.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}{0}{8}{0}{9}{0}{10}{0}{11}{0}{12}{0}{13}{0}{14}",
                '\t',
                FTG_NR,
                P_ID,
                        Namn,
        ANST_NR,
        LoneartID,
        Benamning,
        Antal,
        Belopp,
        Utbdag,
        FakturaNr,
        Statuskod,
        KarNr,
        KarNamn,
        Procent,
        Kst);
        }
    }
}
