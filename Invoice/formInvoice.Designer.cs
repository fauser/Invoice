﻿namespace Fakturering
{
    partial class formInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbCompany = new System.Windows.Forms.ComboBox();
            this.tbInvoiceID = new System.Windows.Forms.TextBox();
            this.buSave = new System.Windows.Forms.Button();
            this.buCancel = new System.Windows.Forms.Button();
            this.buClose = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblInvoiceNbr = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblFilePath = new System.Windows.Forms.Label();
            this.buFilePath = new System.Windows.Forms.Button();
            this.tbFilePath = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbCompany
            // 
            this.cbCompany.FormattingEnabled = true;
            this.cbCompany.Location = new System.Drawing.Point(90, 21);
            this.cbCompany.Name = "cbCompany";
            this.cbCompany.Size = new System.Drawing.Size(227, 21);
            this.cbCompany.TabIndex = 0;
            this.cbCompany.SelectedIndexChanged += new System.EventHandler(this.cbCompany_SelectedIndexChanged);
            // 
            // tbInvoiceID
            // 
            this.tbInvoiceID.Location = new System.Drawing.Point(90, 48);
            this.tbInvoiceID.Name = "tbInvoiceID";
            this.tbInvoiceID.Size = new System.Drawing.Size(227, 20);
            this.tbInvoiceID.TabIndex = 1;
            // 
            // buSave
            // 
            this.buSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buSave.Location = new System.Drawing.Point(23, 12);
            this.buSave.Name = "buSave";
            this.buSave.Size = new System.Drawing.Size(75, 23);
            this.buSave.TabIndex = 4;
            this.buSave.Text = "Spara";
            this.buSave.UseVisualStyleBackColor = true;
            this.buSave.Click += new System.EventHandler(this.buSave_Click);
            // 
            // buCancel
            // 
            this.buCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buCancel.Location = new System.Drawing.Point(23, 205);
            this.buCancel.Name = "buCancel";
            this.buCancel.Size = new System.Drawing.Size(75, 23);
            this.buCancel.TabIndex = 5;
            this.buCancel.Text = "Avbryt";
            this.buCancel.UseVisualStyleBackColor = true;
            this.buCancel.Click += new System.EventHandler(this.buCancel_Click);
            // 
            // buClose
            // 
            this.buClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buClose.Location = new System.Drawing.Point(23, 234);
            this.buClose.Name = "buClose";
            this.buClose.Size = new System.Drawing.Size(75, 23);
            this.buClose.TabIndex = 6;
            this.buClose.Text = "Stäng";
            this.buClose.UseVisualStyleBackColor = true;
            this.buClose.Click += new System.EventHandler(this.buClose_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buSave);
            this.panel1.Controls.Add(this.buCancel);
            this.panel1.Controls.Add(this.buClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(483, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(110, 269);
            this.panel1.TabIndex = 7;
            // 
            // lblInvoiceNbr
            // 
            this.lblInvoiceNbr.AutoSize = true;
            this.lblInvoiceNbr.Location = new System.Drawing.Point(87, 12);
            this.lblInvoiceNbr.Name = "lblInvoiceNbr";
            this.lblInvoiceNbr.Size = new System.Drawing.Size(69, 13);
            this.lblInvoiceNbr.TabIndex = 8;
            this.lblInvoiceNbr.Text = "lblInvoiceNbr";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.lblFilePath);
            this.panel2.Controls.Add(this.buFilePath);
            this.panel2.Controls.Add(this.tbFilePath);
            this.panel2.Location = new System.Drawing.Point(51, 179);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(392, 65);
            this.panel2.TabIndex = 10;
            // 
            // lblFilePath
            // 
            this.lblFilePath.AutoSize = true;
            this.lblFilePath.Location = new System.Drawing.Point(87, 17);
            this.lblFilePath.Name = "lblFilePath";
            this.lblFilePath.Size = new System.Drawing.Size(35, 13);
            this.lblFilePath.TabIndex = 12;
            this.lblFilePath.Text = "label1";
            // 
            // buFilePath
            // 
            this.buFilePath.Location = new System.Drawing.Point(332, 35);
            this.buFilePath.Name = "buFilePath";
            this.buFilePath.Size = new System.Drawing.Size(32, 23);
            this.buFilePath.TabIndex = 11;
            this.buFilePath.Text = "...";
            this.buFilePath.UseVisualStyleBackColor = true;
            // 
            // tbFilePath
            // 
            this.tbFilePath.Location = new System.Drawing.Point(90, 37);
            this.tbFilePath.Name = "tbFilePath";
            this.tbFilePath.Size = new System.Drawing.Size(227, 20);
            this.tbFilePath.TabIndex = 10;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.lblInvoiceNbr);
            this.panel3.Controls.Add(this.tbInvoiceID);
            this.panel3.Location = new System.Drawing.Point(51, 89);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(392, 84);
            this.panel3.TabIndex = 11;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.cbCompany);
            this.panel4.Location = new System.Drawing.Point(51, 29);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(392, 54);
            this.panel4.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Företag";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Fakturanummer";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Sökväg till fil";
            // 
            // formInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 269);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "formInvoice";
            this.Text = "Faktura";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formInvoice_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbCompany;
        private System.Windows.Forms.TextBox tbInvoiceID;
        private System.Windows.Forms.Button buSave;
        private System.Windows.Forms.Button buCancel;
        private System.Windows.Forms.Button buClose;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblInvoiceNbr;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblFilePath;
        private System.Windows.Forms.Button buFilePath;
        private System.Windows.Forms.TextBox tbFilePath;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
    }
}