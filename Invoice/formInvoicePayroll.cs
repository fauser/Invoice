﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fakturering
{
    public partial class formInvoicePayroll : Form
    {
        private bool m_CannotBeClosed;
        private string m_CurrentForetag;
        private string m_CurrentFakturaNr;
        List<int> m_ForetagsID = new List<int>();
        private string m_FranANSTNR;
        private string m_TillANSTNR;

        public formInvoicePayroll()
        {
            InitializeComponent();
            InitValues();
        }

        private void InitValues()
        {
            tbEmployeeIdFrom.Text = "0";
            tbEmployeeIdTo.Text = "999999999999";
            m_CurrentForetag = "3";

            using (SqlConnection connection = new SqlConnection(Helper.ConnectionString()))
            {
                StringBuilder cmd = new StringBuilder();

                cmd.AppendLine("SELECT utb.* FROM BA_UTBGRP utb, BA_TABELL_FTG reg");
                cmd.AppendLine("WHERE reg.FTG_NR = 3");
                cmd.AppendLine(" AND reg.REG_NR = 2");
                cmd.AppendLine(" AND reg.REG_NR_TYP = utb.REG_NR_UTBGRP");
                SqlCommand command = new SqlCommand(cmd.ToString(), connection);

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDataRecord record = reader;
                    Console.WriteLine(record[0]);
                    cbGroup.Items.Add(record.GetString(2));
                    m_ForetagsID.Add(record.GetInt32(0));
                }
            }

            udYear.Maximum = 2100;
            udYear.Minimum = 1965;
            udYear.Increment = 1;
            udYear.Value = 2013;//DateTime.Now.Year;

            cbGroup.SelectedIndex = 0;

            m_CannotBeClosed = false;
        }

        private void menuItemClose_Click(object sender, EventArgs e)
        {
            CloseApplication();
        }

        private void CloseApplication()
        {
            if (m_CannotBeClosed)
            {
                MessageBox.Show("Du måste exportera din låsta fil innan du kan gå ur programmet");
            }
            else
            {
                //m_qu.Free;
                //m_qu:= nil;
                //Close;
            }
            //m_fil.Free;
            //m_ForetagsID.Free;

            //ADOConnection1.Connected:= False;
            Application.Exit();
        }

        private void buClose_Click(object sender, EventArgs e)
        {
            CloseApplication();
        }

        private void buExport_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(Helper.ConnectionString()))
            {
                connection.Open();

                StringBuilder cmd = new StringBuilder();
                cmd.AppendLine("SELECT * FROM BA_FTG_INST WHERE FTG_NR = " + m_CurrentForetag + " AND INSTALLNING = \'EGEN_FRALS_SOKVAG\'");
                SqlCommand command = new SqlCommand(cmd.ToString(), connection);
                SqlDataReader reader = command.ExecuteReader();

                List<String> m_fil = new List<string>();

                if (m_fil.Count > 0)
                {
                    //m_fil.SaveToFile(FieldByName('BESKRIVNING').AsString+'_'+TidStrang+'_'+DateToStr(Date)+'.txt')
                }
                else
                {
                    MessageBox.Show("Det finns ingen fil disponibel!");
                }

                cmd = new StringBuilder();
                cmd.AppendLine("UPDATE TB_EGEN_FRALSFRAKT SET STATUSKOD = 2 WHERE FAKTURANR = " + m_CurrentFakturaNr);
                command = new SqlCommand(cmd.ToString(), connection);
                command.ExecuteNonQuery();
            }

            buLock.Enabled = false;
            buPreview.Enabled = false;
            cbPeriod.Enabled = true;
            cbGroup.Enabled = true;
            buExport.Enabled = false;
            m_CannotBeClosed = false;
        }

        private void buLock_Click(object sender, EventArgs e)
        {
            //Screen.Cursor := CrHourGlass;
            int flagga = 0;
            int LoneperiodID = (int)cbPeriod.Items[cbPeriod.SelectedIndex];
            if (LoneperiodID > -1)
            {

                using (SqlConnection connection = new SqlConnection(Helper.ConnectionString()))
                {
                    connection.Open();

                    StringBuilder cmd = new StringBuilder();
                    cmd.AppendLine("SELECT * FROM BA_UTBGRP_PER WHERE PERIOD_ID = \'" + LoneperiodID + "\' AND PERIOD_STATUS = 3");
                    SqlCommand command = new SqlCommand(cmd.ToString(), connection);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.FieldCount > 0)
                        flagga = 1;
                }

                if (flagga == 0)
                {
                    //         try
                    //           try
                    //             m_fil.Clear;
                    //             //Här ska det läggas ner till tabellen TB_EGEN_FRALSFAKT
                    //             for i:=0 to Length(vvList)-1 do begin
                    //               Personer:= vvList[i];
                    //               if Personer.ANST_NR <> 0 then begin
                    //                 with qu_kar do begin
                    //                   Close;
                    //                   SQL.Clear;
                    //                   SQL.Add('INSERT INTO TB_EGEN_FRALSFRAKT(FTG_NR, P_ID, NAMN, KUNDNR, LONEART_ID, BENAMNING,ANTAL, BELOPP, UTB_DAG, FAKTURANR, STATUSKOD, KAR_NR, KAR_NAMN, PROCENT, KST)');
                    //     //              SQL.Add(' VALUES('+IntToStr(Personer.FTG_NR)+','+IntToStr(Personer.P_ID)+','+chr(39)+Personer.Namn+chr(39)+','+IntToStr(Personer.ANST_NR)+','+IntToStr(Personer.LoneartID)+','+chr(39)+Personer.Benamning+chr(39)+','+FloatToStr(Personer.Antal)+','+FloatToStr(Personer.Belopp)+','+chr(39)+Personer.Utbdag+chr(39)+','+chr(39)+m_CurrentFakturaNr+chr(39)+','+chr(39)+'1'+chr(39)+','+IntToStr(Personer.KarNr)+','+chr(39)+Personer.KarNamn+chr(39)+','+chr(39)+Personer.Procent+chr(39)+')');
                    //                   SQL.Add(' VALUES(:varFTG, :varPID, :varNAMN, :varKUNDNR, :varLONEART_ID, :varBENAMNING, :varANTAL, :varBELOPP, :varUTB_DAG, :varFAKTURANR, :varSTATUSKOD, :varKAR_NR, :varKAR_NAMN, :varPROCENT, :varKST)');

                    //                   Close;
                    //                   Parameters.ParamByName('varFTG').Value := Personer.FTG_NR;
                    //                   Parameters.ParamByName('varPID').Value := Personer.P_ID;
                    //                   Parameters.ParamByName('varNAMN').Value := Personer.Namn;
                    //                   Parameters.ParamByName('varKUNDNR').Value := Personer.ANST_NR;
                    //                   Parameters.ParamByName('varLONEART_ID').Value := Personer.LoneartID;
                    //                   Parameters.ParamByName('varBENAMNING').Value := Personer.Benamning;
                    //                   Parameters.ParamByName('varANTAL').Value := Personer.Antal;
                    //                   Parameters.ParamByName('varBELOPP').Value := Personer.Belopp;
                    //                   Parameters.ParamByName('varUTB_DAG').Value := Personer.Utbdag;
                    //                   Parameters.ParamByName('varFAKTURANR').Value := Personer.FakturaNr;
                    //                   Parameters.ParamByName('varSTATUSKOD').Value := '1';
                    //                   Parameters.ParamByName('varKAR_NR').Value := Personer.KarNr;;
                    //                   Parameters.ParamByName('varKAR_NAMN').Value := Personer.KarNamn;
                    //                   Parameters.ParamByName('varPROCENT').Value := Personer.Procent;
                    //                   Parameters.ParamByName('varKST').Value := Personer.Kst;
                    //                   ExecSQL;

                    //                   // Skapa ny fillayout med det vi sparar.
                    //                   m_fil.Add(IntToStr(Personer.FTG_NR)+delimiter+
                    //                     Personer.Namn+delimiter+
                    //                     IntToStr(Personer.ANST_NR)+delimiter+
                    //                     IntToStr(Personer.LoneartID)+delimiter+
                    //                     Personer.Benamning+delimiter+
                    //                     FloatToStr(Personer.Antal)+delimiter+
                    //                     FloatToStr(Personer.Belopp)+delimiter+
                    //                     Personer.Utbdag+delimiter+
                    //                     Personer.FakturaNr+delimiter+
                    //                     Personer.Statuskod+delimiter+
                    //                     IntToStr(Personer.KarNr)+delimiter+
                    //                     Personer.KarNamn+delimiter+
                    //                     FloatToStr(Personer.Procent)+delimiter+
                    //                     Personer.Kst);
                    //                 end;
                    //               end;//if anstnr <> 0
                    //             end;
                    //           except on e : Exception do
                    //             ShowMessage(e.Message);
                    //           end;
                    //         finally
                    //            qu_kar.Free;
                    //            qu_kar:= nil;
                    //         end;


                }
            }
            else
            {
                //Showmessage('Ingen löneperiod är vald!');
            }

            bool m_FarEjStangas;

            buExport.Enabled = true;
            //buLasfaktura.Enabled:= False;
            buCreateTempInvoice.Enabled = false;
            cbPeriod.Enabled = false;
            cbGroup.Enabled = false;

            if (flagga == 0)
                m_FarEjStangas = true;
            else
                m_FarEjStangas = false;
            //Screen.Cursor := crDefault;
        }

        private void buCreateTempInvoice_Click(object sender, EventArgs e)
        {
            string LoneperiodID = (string)cbPeriod.SelectedItem;

            int currentInvoiceNumber = 0;

            using (SqlConnection connection = new SqlConnection(Helper.ConnectionString()))
            {
                connection.Open();

                StringBuilder cmd = new StringBuilder();

                cmd.AppendLine("SELECT MAX(FAKTURANR)+1 FAKTURANR FROM TB_EGEN_FRALSFRAKT");

                SqlCommand command = new SqlCommand(cmd.ToString(), connection);
                currentInvoiceNumber = (int)command.ExecuteScalar();
            }

            List<InvoiceRow> listInvoioce = new List<InvoiceRow>();

            using (SqlConnection connection = new SqlConnection(Helper.ConnectionString()))
            {
                connection.Open();
                StringBuilder cmd = TempInvoiceCommandString(LoneperiodID);

                SqlCommand command = new SqlCommand(cmd.ToString(), connection);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDataRecord record = reader;
                    InvoiceRow p = CreateInvoiceRow(reader);
                    listInvoioce.Add(p);
                }
            }

            for (int i = 0; i < listInvoioce.Count; i++)
            {
                InvoiceRow ir = listInvoioce[i];

                decimal AntalSemdgr = 0;
                if ((ir.SEM_DAGTYP == "SS") || (ir.SEM_DAGTYP == "SB"))
                    AntalSemdgr = AntalSemdgr + ir.Antal;

                if (ir.SEM_TILLAGG == "N")
                {
                    if (ir.Belopp != 0 &&
                        (ir.LONEARTSTYP == "TB" || ir.LONEARTSTYP == "AB" || ir.LONEARTSTYP == "FV" || ir.LONEARTSTYP == "TN") ||
                        (ir.LONEARTSTYP == "AN" && (ir.LONEART_ID == 928 || ir.LONEART_ID == 969 || ir.LONEART_ID == 971 || ir.LONEART_ID == 974 || ir.LONEART_ID == 979)))
                    {
                        //Why do this? Personer.P_ID is always null att this time
                        //if ((Personer.P_ID <> FieldByName('P_ID').AsInteger) and (Personer.P_ID <> 0)) then begin
                        //  //Detta för att lägga en slutpost för varje person med utgiftstransar.
                        //  Dec(i);//för att komma rätt i pos.
                        //  AnalyseraSemsterUttag(i,Personer, 1004, 'Avdrag uttag semester', AntalSemdgr, ArtDec, Proc,LoneperiodID,Manadslon);
                        //  AnalyseraPersonligaVariabler(i,Personer, 2000, 'FA_adm');
                        //  AnalyseraPersonligaVariabler(i,Personer, 2001, 'FA_myrfond');
                        //  AnalyseraPersonligaVariabler(i,Personer, 2002, 'FA_bidrag_ff');
                        //  AnalyseraPersonligaVariabler(i,Personer, 2003, 'FA_stadstöd');
                        //  AnalyseraPersonligaVariabler(i,Personer, 2004, 'FA_löneavdrag');
                        //  AnalyseraPersonligaVariabler(i,Personer, 2005, 'FA_divavdrag');
                        //  AnalyseraPersonligaVariabler(i,Personer, 2006, 'FA_divtillägg');
                        //  AntalSemdgr:=0;
                        //  //flagga := 1;  //2013-01-18  Används inte någonstans längre
                        //  Inc(i);
                        //  m_CurrentFakturaNr:= IntToStr(StrToInt(m_CurrentFakturaNr)+1);
                        //end;

                        TPersoner person = new TPersoner(ir, currentInvoiceNumber);


                        //Read Name
                        //qu_kar:= TADOQuery.Create(nil);
                        //qu_kar.Connection := ADOConnection1;
                        //qu_kar.SQL.Add('SELECT FORNAMN+'+chr(39)+' '+chr(39)+ '+EFTERNAMN NAMN FROM BA_PERSON');
                        //qu_kar.SQL.Add('WHERE P_ID = '+IntToStr(Personer.P_ID));
                        //qu_kar.Open;
                        //if not qu_kar.eof then
                        //  Personer.Namn:= qu_kar.FieldByName('NAMN').AsString;
                        //qu_kar.Close;
                        //qu_kar:=nil;

                        //Read Core
                        //qu_kar:= TADOQuery.Create(nil);
                        //qu_kar.Connection := ADOConnection1;
                        //with qu_kar do begin
                        //  SQL.Add('SELECT KAR_NR,KAR_NAMN FROM BA_FTG_PERS, TB_EGEN_KAR');
                        //  SQL.Add('WHERE P_ID = '+IntToStr(Personer.P_ID)+' AND ARBSTALLENR = KAR_NR');
                        //  Open;
                        //  if not eof then begin
                        //    Personer.KarNr:= FieldByName('KAR_NR').AsInteger;
                        //    Personer.KarNamn:= trim(FieldByName('KAR_NAMN').AsString);
                        //  end else begin
                        //    Personer.KarNr:= 0;
                        //    Personer.KarNamn:= '';
                        //  end;
                        //  Close;
                        //end;
                        //Personer.Kst:= HamtaKst(Personer.P_ID);
                        //Personer.Antal:= 1;
                        //qu_kar:=nil;


                        if ((ir.ARTDEC == "1" || ir.ARTDEC == "2") && (ir.LONEART_ID == 615 || ir.LONEART_ID == 611))
                        {
                        }
                        else
                        {
                            //  vvList[i]:= Personer;
                            //  m_fil.Add(IntToStr(FieldByName('FTG_NR').AsInteger)+delimiter+Personer.Namn+delimiter+IntToStr(FieldByName('ANST_NR').AsInteger)+delimiter+IntToStr(FieldByName('LONEART_ID').AsInteger)+delimiter+FieldByName('LONEART_TXT').AsString+delimiter+{Antal}'1'+delimiter+Belopp+delimiter+FieldByName('UTBET_DATUM').AsString+delimiter+m_CurrentFakturaNr+delimiter+'0'+delimiter+IntToStr(Personer.KarNr)+delimiter+Personer.KarNamn+delimiter+'0'+delimiter+Personer.Kst);
                        }


                        decimal manadslon = 0;

                        if (ir.LONEARTSTYP != "AN" && ir.LONEARTSTYP != "TN" && ir.LONEARTSTYP != "FV")
                        {
                            if (ir.USOC == "J")
                            {
                                if (ir.ARTDEC == "1" || (ir.ARTDEC == "2" && (ir.LONEART_ID == 615 || ir.LONEART_ID == 611)))
                                { }
                                else
                                {
                                    if (ir.SOCAVG_TXT.ToLower() == "sociala avgifter")
                                    {
                                        double proc = PlockaUtSocialaAVG(person.P_ID, person.Utbdag);
                                        //      AnalyseraSocialaAvgifter(i,Personer,FieldByName('LOP_NR_LONEART').AsInteger, 1000, 'Sociala avgifter enl. lag', Proc);
                                        proc = PlockaUtSocialaAVG(person.P_ID, person.Utbdag);
                                        //      AnalyseraSocialaAvgifterKonstant(i,Personer, 1001, 'Sociala avgifter avtal',FieldByName('ARTDEC').AsString);
                                    }
                                }

                                if (ir.ARTDEC == "2" && person.LoneartID == 70)
                                {
                                    manadslon = person.Belopp;
                                    //    AnalyseraSemsterErs(i,Personer, 1002, 'Semesterers 1 mån', FieldByName('LOP_NR_LONEART').AsInteger, FieldByName('ARTDEC').AsString, Proc);
                                }

                                if (ir.LONEARTSTYP == "FV")
                                {
                                    if (ir.LONEART_ID == 694 || ir.LONEART_ID == 693)
                                    {
                                        double proc = PlockaUtSocialaAVG(person.P_ID, person.Utbdag);
                                        //    AnalyseraSocialaAvgifter(i,Personer,FieldByName('LOP_NR_LONEART').AsInteger, 1000, 'Sociala avgifter enl. lag', Proc);
                                    }
                                    else
                                    {
                                        double proc = PlockaUtSocialaAVG(person.P_ID, person.Utbdag);
                                        //    AnalyseraSocialaAvgifterForman(i,Personer,FieldByName('LOP_NR_LONEART').AsInteger, 1000, 'Sociala avgifter enl. lag(Förmån)', Proc);
                                    }
                                }
                            }
                        }


                    }
                }


            }

            //AnalyseraSemsterUttag(i,Personer, 1004, 'Avdrag uttag semester', AntalSemdgr, ArtDec, Proc,LoneperiodID,Manadslon);
            //AnalyseraPersonligaVariabler(i,Personer, 2000, 'FA_adm');
            //AnalyseraPersonligaVariabler(i,Personer, 2001, 'FA_myrfond');
            //AnalyseraPersonligaVariabler(i,Personer, 2002, 'FA_bidrag_ff');
            //AnalyseraPersonligaVariabler(i,Personer, 2003, 'FA_stadstöd');
            //AnalyseraPersonligaVariabler(i,Personer, 2004, 'FA_löneavdrag');
            //AnalyseraPersonligaVariabler(i,Personer, 2005, 'FA_divavdrag');
            //AnalyseraPersonligaVariabler(i,Personer, 2006, 'FA_divtillägg');


            //Visaknappar(True);
            //Screen.Cursor := CrDefault;
            buPreview.Enabled = false;
        }

        private static InvoiceRow CreateInvoiceRow(SqlDataReader reader)
        {
            InvoiceRow p = new InvoiceRow();
            p.P_ID = reader.GetInt32((int)EnumInvoiceRowIndex.P_ID);
            p.FTG_NR = reader.GetInt16((int)EnumInvoiceRowIndex.FTG_NR);
            p.ANST_NR = reader.GetDecimal((int)EnumInvoiceRowIndex.ANST_NR);
            p.Antal = reader.GetDecimal((int)EnumInvoiceRowIndex.ANTAL);
            p.Belopp = reader.GetDecimal((int)EnumInvoiceRowIndex.BELOPP);
            p.LOP_NR_LONEART = reader.GetInt32((int)EnumInvoiceRowIndex.LOP_NR_LONEART);
            p.LONEART_TXT = reader.GetString((int)EnumInvoiceRowIndex.LONEART_TXT);
            p.LONEART_ID = reader.GetInt32((int)EnumInvoiceRowIndex.LONEART_ID);
            p.Utbdag = reader.GetString((int)EnumInvoiceRowIndex.UTBET_DATUM);
            p.SOCAVG_TXT = reader.GetString((int)EnumInvoiceRowIndex.SOCAVG_TXT);
            p.SOCAVG_PROCENT = reader.GetDecimal((int)EnumInvoiceRowIndex.SOCAVG_PROCENT);
            p.ARTDEC = reader.GetString((int)EnumInvoiceRowIndex.ARTDEC);
            p.USOC = reader.GetString((int)EnumInvoiceRowIndex.USOC);
            p.SEM_DAGTYP = reader.GetString((int)EnumInvoiceRowIndex.SEM_DAGTYP);
            p.LONEARTSTYP = reader.GetString((int)EnumInvoiceRowIndex.LONEARTSTYP);
            p.RAD_ID = reader.GetInt32((int)EnumInvoiceRowIndex.RAD_ID);
            p.SEM_TILLAGG = reader.GetString((int)EnumInvoiceRowIndex.SEM_TILLAGG);
            return p;
        }

        private StringBuilder TempInvoiceCommandString(string LoneperiodID)
        {
            StringBuilder cmd = new StringBuilder();

            cmd.AppendLine("SELECT DISTINCT");
            cmd.AppendLine("LONETRANS.P_ID");
            cmd.AppendLine(",LONETRANS.FTG_NR");
            cmd.AppendLine(",PERS.ANST_NR");
            cmd.AppendLine(",CASE WHEN PERTRANS.ANTAL<>0 THEN PERTRANS.OMRANTAL ELSE 0 END AS ANTAL");
            cmd.AppendLine(",PERTRANS.BELOPP");
            cmd.AppendLine(",LONEART.LOP_NR_LONEART");
            cmd.AppendLine(",LONEART.LONEART_TXT");
            cmd.AppendLine(",LONEART.LONEART_ID");
            cmd.AppendLine(",UTB.UTBET_DATUM");
            cmd.AppendLine(",SOC.SOCAVG_TXT");
            cmd.AppendLine(",SOC.SOCAVG_PROCENT");
            cmd.AppendLine(",PERS.ARTDEC");
            cmd.AppendLine(",LONEART.USOC");
            cmd.AppendLine(",LONEART.SEM_DAGTYP");
            cmd.AppendLine(",LONEART.LONEARTSTYP");
            cmd.AppendLine(",PERTRANS.RAD_ID");
            cmd.AppendLine(",LONEART.SEM_TILLAGG");
            cmd.AppendLine("FROM BA_LONPER_PERS LONETRANS, BA_LONPERTRANS PERTRANS, BA_LONART LONEART, BA_FTG_PERS PERS,BA_UTBGRP_PER UTB,BA_SOCIALA SOC");
            cmd.AppendLine("WHERE LONETRANS.PERIOD_ID= '" + LoneperiodID + "'");
            cmd.AppendLine("AND PERTRANS.LOP_NR_PER_PERSON = LONETRANS.LOP_NR_PER_PERSON");
            cmd.AppendLine("AND PERTRANS.LOP_NR_LONEART = LONEART.LOP_NR_LONEART");
            cmd.AppendLine("AND LONETRANS.P_ID = PERS.P_ID");
            cmd.AppendLine("AND PERS.FTG_NR = 3");
            cmd.AppendLine("AND LONETRANS.PERIOD_ID = UTB.PERIOD_ID");
            cmd.AppendLine("AND LONETRANS.LOP_NR_UTBGRP = UTB.LOP_NR_UTBGRP");
            cmd.AppendLine("AND LONETRANS.FTG_NR=UTB.FTG_NR");
            cmd.AppendLine("AND LONETRANS.LOP_NR_UTBGRP = 13"); //Get lop_nr_utbgrp from cbGroup, not the text
            cmd.AppendLine("AND PERS.SOCIALA_AVGIFTER = SOC.LOP_NR_SOCAVG");
            cmd.AppendLine("AND PERS.ANST_NR BETWEEN " + tbEmployeeIdFrom.Text + " AND " + tbEmployeeIdTo.Text);
            cmd.AppendLine("ORDER BY PERS.ANST_NR");
            return cmd;
        }

        private void buPreview_Click(object sender, EventArgs e)
        {
            /*

                  try
        buPrelfaktura.Enabled := False;
        Granskning := TFGranskning.Create(m_fil);
        Granskning.ShowModal;
        Granskning.Free;
      except
        on e : Exception do
          ShowMessage(e.Message);
      end;
            */

        }

        private void cbPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
             
                procedure TFLonefaktura.cbLoneperiodChange(Sender: TObject);
    begin
      VisaKnappar(False);
      buPrelfaktura.Enabled:= True;
      buFil.Enabled:= False;
    end;
             
            */
        }

        private void menuItemSettings_Click(object sender, EventArgs e)
        {
            formInvoice frmInv = new formInvoice();
            frmInv.ShowDialog();
        }

        private void cbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {

            using (SqlConnection connection = new SqlConnection(Helper.ConnectionString()))
            {
                connection.Open();

                StringBuilder cmd = new StringBuilder();
                cmd.AppendLine("Select DISTINCT PERIOD_ID From BA_UTBGRP_PER WHERE LOP_NR_UTBGRP = " + m_ForetagsID[cbGroup.SelectedIndex]);
                cmd.AppendLine(" AND LEFT(FOM_DATUM,4)=\'" + udYear.Value.ToString() + "\'");

                SqlCommand command = new SqlCommand(cmd.ToString(), connection);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    cbPeriod.Items.Add(reader.GetString(0));
                }
            }
            cbPeriod.Enabled = true;
            cbPeriod.SelectedIndex = 0;


            /*
            procedure TFLonefaktura.cbForetagChange(Sender: TObject);
    var
      qu: TADOQuery;
    begin
      qu:=nil;  //2013-01-18  Måste initieras annars ges en warning
      cbLoneperiod.Clear;
      try
        try
          qu:= TADOQuery.Create(nil);
          qu.Connection := ADOConnection1;
          m_CurrentForetag:= '3';//m_ForetagsID.Strings[cbForetag.ItemIndex];
          with qu Do Begin

            SQL.Clear;
            SQL.Add('Select DISTINCT PERIOD_ID From BA_UTBGRP_PER WHERE LOP_NR_UTBGRP = '+m_ForetagsID.Strings[cbForetag.ItemIndex]);
            SQL.Add(' AND LEFT(FOM_DATUM,4)='+chr(39)+edAr.Text+chr(39));
            Open;
            if not eof then begin
              while not eof do begin
                cbLoneperiod.Items.Add(FieldByName('PERIOD_ID').AsString);//(FieldByName('FOM_DATUM').AsString + ' - ' + FieldByName('TOM_DATUM').AsString);
                Next;
              end;
              cbLoneperiod.Enabled:= True;
            end;
            Close;
          end;

          with qu Do Begin

            SQL.Clear;
            SQL.Add('SELECT * FROM BA_FTG_INST WHERE FTG_NR='+m_CurrentForetag+' AND INSTALLNING = '+chr(39)+'EGEN_FRALS_SOKVAG'+chr(39));
            Open;
            if not eof then begin
              m_CurrentFakturaNr:= FieldByName('VARDE').AsString;
            end;
            Close;
          end;

         except on e : Exception do
          ShowMessage(e.Message);
         end;
        finally
          qu.Free;
          qu:= nil;
        end;
        edFakturaSok.Enabled:= True;
        buSokkod.Enabled:= True;
    end;
            */

        }

        private void udYear_ValueChanged(object sender, EventArgs e)
        {
            /*
            
    procedure TFLonefaktura.upNavigeraArClick(Sender: TObject; Button: TUDBtnType);
    begin
      if cbForetag.ItemIndex <> -1 then begin
        cbLoneperiod.Clear;
        cbForetagChange(self);
      end;
    end;

            */
        }

        private void buSearch_Click(object sender, EventArgs e)
        {
            List<TPersoner> listPersoner = new List<TPersoner>();
            using (SqlConnection connection = new SqlConnection(Helper.ConnectionString()))
            {
                connection.Open();

                StringBuilder cmd = new StringBuilder();
                cmd.AppendLine("SELECT P_ID,FTG_NR,NAMN,KUNDNR,LONEART_ID,BENAMNING,ANTAL,BELOPP,UTB_DAG,"
                + "FAKTURANR,STATUSKOD,KAR_NR,KAR_NAMN,PROCENT,KST from TB_EGEN_FRALSFRAKT where FAKTURANR =\'" + tbSearchInvoice.Text + "\'");

                SqlCommand command = new SqlCommand(cmd.ToString(), connection);
                SqlDataReader reader = command.ExecuteReader();



                while (reader.Read())
                {
                    TPersoner p = new TPersoner();

                    p.P_ID = reader.GetInt32(0);
                    p.FTG_NR = reader.GetInt32(1);
                    p.Namn = reader.GetString(2).Trim();
                    p.ANST_NR = reader.GetDecimal(3);
                    p.LoneartID = reader.GetInt32(4);
                    p.Benamning = reader.GetString(5).Trim();
                    p.Antal = reader.GetDecimal(6);
                    p.Belopp = reader.GetDecimal(7);
                    p.Utbdag = reader.GetString(8).Trim();
                    p.FakturaNr = reader.GetString(9).Trim();
                    p.Statuskod = reader.GetString(10).Trim();
                    p.KarNr = reader.GetInt32(11);
                    p.KarNamn = reader.GetString(12).Trim();
                    p.Procent = reader.GetDecimal(13);
                    p.Kst = reader.GetString(14).Trim();

                    listPersoner.Add(p);
                }
            }

            buPreview.Enabled = false;
            buLock.Enabled = false;
            m_FranANSTNR = "0";
            m_TillANSTNR = "9999999";
            formPreview Granskning = new formPreview(new List<string>(), listPersoner);
            Granskning.ShowDialog();

            /*
            procedure TFLonefaktura.buSokkodClick(Sender: TObject);
    var
      Personer: TPersoner;
      i: Integer;
      Granskning: TFGranskning;
      Bel, Ant, proc: Real;
      qu: TADOQuery;
    begin
      qu:=nil;  //2013-01-18  Måste initieras annars ges en warning
      i:= 0;
       try
        qu:= TADOQuery.Create(nil);
        qu.Connection := ADOConnection1;
         try
          with qu do begin
            SQL.Clear;
            SQL.Add('SELECT P_ID,FTG_NR,NAMN,KUNDNR,LONEART_ID,BENAMNING,ANTAL,BELOPP,UTB_DAG,FAKTURANR,STATUSKOD,KAR_NR,KAR_NAMN,PROCENT,KST from TB_EGEN_FRALSFRAKT where FAKTURANR ='+chr(39)+edFakturaSok.Text+chr(39));
    //        SQL.SAveTOFile('C:/HEYSK.txt');
            Open;
            if not eof then begin
              if (MessageDlg('All annan faktura information som inte är låst eller exporterad kommer att försvinna, är det ok?',mtConfirmation, [mbYes, mbCancel],0) = mrYes) then begin
                SetLength(vvList,qu.RecordCount);
                while not eof do begin
                  Personer.P_ID:= FieldByName('P_ID').AsInteger;
                  Personer.FTG_NR:= FieldByName('FTG_NR').AsInteger;
                  Personer.Namn:= trim(FieldByName('NAMN').AsString);
                  Personer.ANST_NR:= FieldByName('KUNDNR').AsInteger;
                  Personer.LoneartID:= FieldByName('LONEART_ID').AsInteger;
                  Personer.Benamning:= trim(FieldByName('BENAMNING').AsString);
                  Ant := FieldByName('ANTAL').AsFloat;
                  Bel:= FieldByName('BELOPP').AsFloat;
                  Personer.Antal:= StrToFloat(FormatFloat('0.00',Ant));
                  Personer.Belopp:= StrToFloat(FormatFloat('0.00',Bel));//FieldByName('BELOPP').AsFloat;
                  Personer.Utbdag:= FieldByName('UTB_DAG').AsString;
                  Personer.FakturaNr:= trim(FieldByName('FAKTURANR').AsString);
                  Personer.Statuskod:= FieldByName('STATUSKOD').AsString;
                  Personer.KarNr:= FieldByName('KAR_NR').AsInteger;
                  Personer.KarNamn:= trim(FieldByName('KAR_NAMN').AsString);
                  Proc:= FieldByName('PROCENT').AsFloat;
                  Personer.Procent:= StrToFloat(FormatFloat('0.00',Proc));
                  Personer.Kst:= FieldByName('KST').AsString;
                  vvList[i]:= Personer;
                  Inc(i);
                  Next;
                end;
                buGranska.Enabled:= False;
                buLasfaktura.Enabled:= False;
                m_FranANSTNR:= '0';
                m_TillANSTNR:= '9999999';
                Granskning := TFGranskning.Create(nil);
                Granskning.ShowModal;
                Granskning.Free;
              end;
            end;
            Close;
          end;
         except
          on e:exception do Showmessage(e.Message);
         end;
       finally
        qu.Free;
        qu:= nil;
       end;
    end;

            */

        }

        private void menuItemConvertion_Click(object sender, EventArgs e)
        {

            formMapping mapping = new formMapping();
            mapping.ShowDialog();

            /*
            procedure TFLonefaktura.mvOversattningClick(Sender: TObject);
    var
      Oversattning: TFOversattning;
      qu: TADOQuery;
    begin
      qu:=nil;  //2013-01-18  Måste initieras annars ges en warning
      try
        qu:= TADOQuery.Create(nil);
        qu.Connection := ADOConnection1;
        Oversattning := TFOversattning.Create(qu);
        Oversattning.ShowModal;
        Oversattning.Free;
      finally
        qu.Free;
        qu:= nil;
      end;

    end;

            */
        }

        private void VisaKnappar(bool bbrowse)
        {
            /*
                  gbUrvalANSTNR.Enabled:= not bbrowse;
      lbFran.Enabled:=  not bbrowse;
      lbTill.Enabled:= not bbrowse;
      edFran.Enabled:= not bbrowse;
      edTill.Enabled:= not bbrowse;
      buGranska.Enabled:= bbrowse;
      buLasfaktura.Enabled := bbrowse;
            */
        }

        private void AnalyseraSocialaAvgifter(ref int i, ref TPersoner Personer, int lop_nr_loneart, int ID, string Socavg, double Procent)
        {
            /*
            procedure TFLonefaktura.AnalyseraSocialaAvgifter(var i: Integer;
      var Personer: TPersoner;lop_nr_loneart:Integer; ID:Integer; Socavg: String; Procent: Double);
    const delimiter = #9;
    var
      //j: Integer;  //2013-01-18  Används aldrig
      qu: TADOQuery;
      Pers: TPersoner;
      Proctmp: String;
      //Procenten: String;  //2013-01-18  Används aldrig
      Belopp,Belopptmp: String;
    begin
      qu:=nil;  //2013-01-18  Måste initieras annars ges en warning
      try
        //j:=i;  //2013-01-18  Används aldrig
        qu:= TADOQuery.Create(nil);
        qu.Connection := ADOConnection1;
        with qu do begin
          SQL.Add('SELECT * FROM BA_LONART WHERE LOP_NR_LONEART = '+IntToStr(lop_nr_loneart));
          Open;
          if not eof then begin
            if FieldByName('USOC').AsString = 'J' then begin //underlag sociala avg. loneartreg.
              Pers.FTG_NR:= Personer.FTG_NR;
              Pers.P_ID:= Personer.P_ID;
              Pers.Namn:= Personer.Namn;
              Pers.ANST_NR:= Personer.ANST_NR;
              Pers.LoneartID:= ID;
              Pers.Benamning:= Socavg;
              Pers.Antal:= 1;
              Pers.Belopp:= StrToFloat(FormatFloat('0.00',Personer.Belopp * (Procent/100)));//Summan av de sociala avg. på löenarten
              Belopptmp := FloatToStr(Pers.Belopp);
              Belopp:= StringReplace(Belopptmp,'.',',',[rfReplaceAll]);
              Pers.Utbdag:= Personer.Utbdag;
              Pers.FakturaNr:= Personer.FakturaNr;
              Pers.Statuskod:= Personer.Statuskod;
              Pers.KarNr:= Personer.KarNr;
              Pers.KarNamn:= Personer.KarNamn;

              Pers.Procent:= Procent;
              Pers.Kst:= Personer.Kst;
              Proctmp := FLoatToStr(Procent);
              //Procenten:= StringReplace(Proctmp,',','.',[rfReplaceAll]);

              m_fil.Add(IntToStr(Pers.FTG_NR)+delimiter+Pers.Namn+delimiter+IntToStr(Pers.ANST_NR)+delimiter+IntToStr(Pers.LoneartID)+delimiter+Pers.Benamning+delimiter+FloatToStr(Pers.Antal)+delimiter+Belopp+delimiter+Pers.Utbdag+delimiter+Pers.FakturaNr+delimiter+'0'+delimiter+IntToStr(Pers.KarNr)+delimiter+Pers.KarNamn+delimiter+Proctmp+delimiter+Pers.Kst);
              inc(m_Counter);
              SetLength(vvList,m_Counter);
              inc(i);
              vvList[i]:= Pers;
            end;
          end;
          Close;
        end;
      finally
        qu.Free;
        qu:= nil;
      end;
    end;
            */

        }

        private void AnalyseraSocialaAvgifterKonstant(ref int i, ref TPersoner Personer, int ID, string Socavg, string Art_dec)
        {
            /*
            procedure TFLonefaktura.AnalyseraSocialaAvgifterKonstant(var i: Integer;
      var Personer: TPersoner; ID:Integer; Socavg: String;Art_dec: String);
    const delimiter = #9;
    var
      //j: Integer;  //2013-01-18  Används aldrig
      qu: TADOQuery;
      Pers: TPersoner;
      Proctmp: String;
      //Procenten: String;  //2013-01-18  Används aldrig
      Belopp,Belopptmp: String;
      Varde: Double;
    begin
      qu:=nil;  //2013-01-18  Måste initieras annars ges en warning
      try
        //j:=i;  //2013-01-18  Används aldrig
        qu:= TADOQuery.Create(nil);
        qu.Connection := ADOConnection1;
        with qu do begin
          SQL.Add('SELECT VARDE FROM BA_VARIABEL WHERE VARIABEL_TXT = '+chr(39)+'FA_avtförs_'+Art_dec+chr(39));
          //SQL.SAveTOFile('C:/HEYSK.txt');
          Open;
          if not eof then begin
            Varde := FieldByName('VARDE').AsFloat;
            Pers.FTG_NR:= Personer.FTG_NR;
            Pers.P_ID:= Personer.P_ID;
            Pers.Namn:= Personer.Namn;
            Pers.ANST_NR:= Personer.ANST_NR;
            Pers.LoneartID:= ID;
            Pers.Benamning:= Socavg;
            Pers.Antal:= 1;
            Pers.Belopp:= StrToFloat(FormatFloat('0.00',Personer.Belopp * (Varde/100)));//Summan av de sociala avg. på löenarten
            Belopptmp := FloatToStr(Pers.Belopp);
            Belopp:= StringReplace(Belopptmp,'.',',',[rfReplaceAll]);
            Pers.Utbdag:= Personer.Utbdag;
            Pers.FakturaNr:= Personer.FakturaNr;
            Pers.Statuskod:= Personer.Statuskod;
            Pers.KarNr:= Personer.KarNr;
            Pers.KarNamn:= Personer.KarNamn;


            Proctmp := FLoatToStr(Varde);
            //Procenten:= StringReplace(Proctmp,',','.',[rfReplaceAll]);
            Pers.Procent:= Varde;
            Pers.Kst:= Personer.Kst;

            if Pers.Belopp <> 0 then begin
              m_fil.Add(IntToStr(Pers.FTG_NR)+delimiter+Pers.Namn+delimiter+IntToStr(Pers.ANST_NR)+delimiter+IntToStr(Pers.LoneartID)+delimiter+Pers.Benamning+delimiter+FloatToStr(Pers.Antal)+delimiter+Belopp+delimiter+Pers.Utbdag+delimiter+Pers.FakturaNr+delimiter+'0'+delimiter+IntToStr(Pers.KarNr)+delimiter+Pers.KarNamn+delimiter+Proctmp+delimiter+Pers.Kst);
              inc(m_Counter);
              SetLength(vvList,m_Counter);
              inc(i);
              vvList[i]:= Pers;

              AnalyseraLoneskatt(i, Personer, 1006, 'Sociala avgifter löneskatt', Pers.Belopp);
            end;
          end;
        end;
      finally
        qu.Free;
        qu:= nil;
      end;

    end;
            */
        }

        private void AnalyseraLoneskatt(ref int i, ref TPersoner Personer, int ID, string Benamning, double Bel)
        {
            /*
            procedure TFLonefaktura.AnalyseraLoneskatt(var i: Integer;
      var Personer: TPersoner; ID:Integer; Benamning: String;Bel: Double);
    const delimiter = #9;
    var
      qu: TADOQuery;
      Pers: TPersoner;
      Proctmp: String;
      Belopp,Belopptmp: String;
      Varde: Double;
    begin
      qu:=nil;  //2013-01-18  Måste initieras annars ges en warning
      try
        qu:= TADOQuery.Create(nil);
        qu.Connection := ADOConnection1;
        with qu do begin
          SQL.Add('SELECT VARDE FROM BA_VARIABEL WHERE VARIABEL_TXT = '+chr(39)+'FA_avtförs_Löneskatt'+chr(39));
          Open;
          if not eof then begin
            Varde := FieldByName('VARDE').AsFloat;
            Pers.FTG_NR:= Personer.FTG_NR;
            Pers.P_ID:= Personer.P_ID;
            Pers.Namn:= Personer.Namn;
            Pers.ANST_NR:= Personer.ANST_NR;
            Pers.LoneartID:= ID;
            Pers.Benamning:= Benamning;
            Pers.Antal:= 1;
            Pers.Belopp:= StrToFloat(FormatFloat('0.00',Bel * (Varde/100)));
            Belopptmp := FloatToStr(Pers.Belopp);
            Belopp:= StringReplace(Belopptmp,'.',',',[rfReplaceAll]);
            Pers.Utbdag:= Personer.Utbdag;
            Pers.FakturaNr:= Personer.FakturaNr;
            Pers.Statuskod:= Personer.Statuskod;
            Pers.KarNr:= Personer.KarNr;
            Pers.KarNamn:= Personer.KarNamn;

            Proctmp := FLoatToStr(Varde);
            Pers.Procent:= Varde;
            Pers.Kst:= Personer.Kst;

            if Pers.Belopp <> 0 then begin
              m_fil.Add(IntToStr(Pers.FTG_NR)+delimiter+Pers.Namn+delimiter+IntToStr(Pers.ANST_NR)+delimiter+IntToStr(Pers.LoneartID)+delimiter+Pers.Benamning+delimiter+FloatToStr(Pers.Antal)+delimiter+Belopp+delimiter+Pers.Utbdag+delimiter+Pers.FakturaNr+delimiter+'0'+delimiter+IntToStr(Pers.KarNr)+delimiter+Pers.KarNamn+delimiter+Proctmp+delimiter+Pers.Kst);
              inc(m_Counter);
              SetLength(vvList,m_Counter);
              inc(i);
              vvList[i]:= Pers;
            end;
          end;
          Close;
        end;
      finally
        qu.Free;
        qu:= nil;
      end;

    end;

            */
        }

        private void AnalyseraPersonligaVariabler(ref int i, ref TPersoner Personer, int ID, string Variabel)
        {
            /*
             procedure TFLonefaktura.AnalyseraPersonligaVariabler(var i: Integer;
      var Personer: TPersoner; ID: Integer; Variabel: String);
    const delimiter = #9;
    var
      //j: Integer;
      qu, qu_temp: TADOQuery;
      Pers: TPersoner;
      Belopp,Belopptmp: String;
    begin
      qu:=nil;  //2013-01-18  Måste initieras annars ges en warning
      qu_temp:=nil;  //2013-01-18  Måste initieras annars ges en warning
      try
        //j:=i;  //2013-01-18  Används aldrig
        qu:= TADOQuery.Create(nil);
        qu.Connection := ADOConnection1;
        with qu do begin
          SQL.Add('SELECT p.VARDE,v.BESKRIVNING FROM BA_VARIABEL v, ba_pers_persvar p WHERE VARIABEL_TXT = '+chr(39)+Variabel+chr(39));
          SQL.Add(' AND v.lop_nr_vartyp = 6');
          SQL.Add(' AND p.lop_nr_variabel = v.lop_nr_variabel');
          SQL.Add(' AND p.P_ID = '+IntToStr(Personer.P_ID));
          Open;
          if not eof then begin
            Pers.Benamning:= FieldByName('BESKRIVNING').AsString;
            Pers.Belopp:= StrToFloat(FormatFloat('0.00',FieldByName('VARDE').AsFloat));

          end else begin
            try
              qu_temp:= TADOQuery.Create(nil);
              qu_temp.Connection := ADOConnection1;
              qu_temp.SQL.Add('SELECT BESKRIVNING FROM BA_VARIABEL WHERE VARIABEL_TXT = '+chr(39)+Variabel+chr(39));
              qu_temp.Open;
              if not qu_temp.Eof then begin
                Pers.Benamning:= qu_temp.FieldByName('BESKRIVNING').AsString;
                Pers.Belopp:= 0.00;
              end;
              qu_temp.Close;
            finally
              qu_temp.Free;
              qu_temp:= nil;
            end;
          end;
          Close;
        end;
        Pers.FTG_NR:= Personer.FTG_NR;
        Pers.P_ID:= Personer.P_ID;
        Pers.Namn:= Personer.Namn;
        Pers.ANST_NR:= Personer.ANST_NR;
        Pers.LoneartID:= ID;
        Pers.Antal:= 1;
        Belopptmp := FloatToStr(Pers.Belopp);
        Belopp:= StringReplace(Belopptmp,'.',',',[rfReplaceAll]);
        Pers.Utbdag:= Personer.Utbdag;
        Pers.FakturaNr:= Personer.FakturaNr;
        Pers.Statuskod:= Personer.Statuskod;
        Pers.KarNr:= Personer.KarNr;
        Pers.KarNamn:= Personer.KarNamn;
        Pers.Kst:= Personer.Kst;
        if Pers.Belopp <> 0 then begin
          m_fil.Add(IntToStr(Pers.FTG_NR)+delimiter+Pers.Namn+delimiter+IntToStr(Pers.ANST_NR)+delimiter+IntToStr(Pers.LoneartID)+delimiter+Pers.Benamning+delimiter+FloatToStr(Pers.Antal)+delimiter+Belopp+delimiter+Pers.Utbdag+delimiter+Pers.FakturaNr+delimiter+'0'+delimiter+IntToStr(Pers.KarNr)+delimiter+Pers.KarNamn+delimiter+'0'+delimiter+Pers.Kst);
          inc(m_Counter);
          SetLength(vvList,m_Counter);
          inc(i);
          vvList[i]:= Pers;
        end;

      finally
        qu.Free;
        qu:= nil;
      end;
    end;


            */
        }

        private void AnalyseraSemsterErs(ref int i, ref TPersoner Personer, int ID, string Variabel, int Lop_Nr_Loneart, string Art_dec, double SocavgProcent)
        {
            /*
            procedure TFLonefaktura.AnalyseraSemsterErs(var i: Integer; var Personer: TPersoner;
                ID: Integer; Variabel: String; Lop_Nr_Loneart: Integer; Art_dec: String; SocavgProcent: Double);
                const
                  delimiter = #9;
                var
                  //qu: TADOQuery;
                  quSem: TADOQuery;
                  semers, semdgr, semratt: Double;
                  Pers: TPersoner;
                  Belopp,Belopptmp: String;
                begin
                  quSem:=nil;  //2013-01-18  Måste initieras annars ges en warning
                  semers:=0;  //2013-01-18  Måste initieras annars ges en warning
                  semdgr:=0;  //2013-01-18  Måste initieras annars ges en warning
                  try
                    try
                      quSem:= TADOQuery.Create(nil);
                      quSem.Connection := ADOConnection1;
                      with quSem do begin
                        SQL.Add('SELECT SEMRATT FROM BA_FTG_PERS WHERE P_ID = '+IntToStr(Personer.P_ID));
                        Open;
                        if not eof then begin
                          semratt:= FieldByName('SEMRATT').AsFloat;
                          semdgr:= StrToFloat(FormatFloat('0.00000',(semratt/12)/100));
                        end;
                        CLose;
                      end;
                    finally
                      quSem.Free;
                      quSem:= nil;
                    end;
                //    qu:= TADOQuery.Create(nil);
                //    qu.Connection := ADOConnection1;
                //    with qu do begin
                //      SQL.Add('SELECT * FROM BA_VAR_LONART WHERE LOP_NR_LONEART ='+IntToStr(Lop_Nr_Loneart));
                //      Open;
                //      if not eof then begin
                //        if Art_dec = '1' then begin
                //          //semers:= 0.04557;  //2013-01-18 Artdec 1 ska inte längre räkna nått tillägg
                //          //semdgr:= 0.0267;
                //        end
                        if Art_dec = '2' then begin
                          semers:= 0.054;
                          //semdgr:= 0.0225;  /* Beräknas ovan via semrätt istället
                        end;
                        Pers.FTG_NR:= Personer.FTG_NR;
                        Pers.P_ID:= Personer.P_ID;
                        Pers.Namn:= Personer.Namn;
                        Pers.ANST_NR:= Personer.ANST_NR;
                        Pers.LoneartID:= ID;
                        Pers.Benamning:= Variabel;
                        Pers.Antal:= 1;
                        Pers.Belopp:= StrToFloat(FormatFloat('0.00',(Personer.Belopp * semers * semdgr) * 100));
                        Belopptmp := FloatToStr(Pers.Belopp);
                        Belopp:= StringReplace(Belopptmp,'.',',',[rfReplaceAll]);
                        Pers.Utbdag:= Personer.Utbdag;
                        Pers.FakturaNr:= Personer.FakturaNr;
                        Pers.Statuskod:= Personer.Statuskod;
                        Pers.KarNr:= Personer.KarNr;
                        Pers.KarNamn:= Personer.KarNamn;
                        Pers.Kst:= Personer.Kst;

                        m_fil.Add(IntToStr(Pers.FTG_NR)+delimiter+Pers.Namn+delimiter+IntToStr(Pers.ANST_NR)+delimiter+IntToStr(Pers.LoneartID)+delimiter+Pers.Benamning+delimiter+FloatToStr(Pers.Antal)+delimiter+Belopp+delimiter+Pers.Utbdag+delimiter+Pers.FakturaNr+delimiter+'0'+delimiter+IntToStr(Pers.KarNr)+delimiter+Pers.KarNamn+delimiter+'0'+delimiter+Pers.Kst);
                        inc(m_Counter);
                        SetLength(vvList,m_Counter);
                        inc(i);
                        vvList[i]:= Pers;

                //        //  Påslag av extra avgift på sociala. 2013-01-18 Ska inte längre vara nått påslag utan bara sociala
                //        try
                //          quSem:= TADOQuery.Create(nil);
                //          quSem.Connection := ADOConnection1;
                //          with quSem do begin
                //            SQL.Add('SELECT VARDE FROM BA_VARIABEL WHERE VARIABEL_TXT = '+chr(39)+'FA_avtförs_'+Art_dec+chr(39));
                //            Open;
                //            if not eof then begin
                //              SocavgProcent:= SocavgProcent + FieldByName('VARDE').AsFloat;
                //            end;
                //            Close;
                //          end;
                //        finally
                //          quSem.Free;
                //          quSem:= nil;
                //        end;
                        AnalyseraSocialaAvgifter(i,Pers,Lop_Nr_Loneart, 1003, 'Sociala avgifter semesterkassa', SocavgProcent);

                    //  end;  
                    //  Close;
                    //end;  
                  finally
                    //qu.Free;
                    //qu:= nil;
                  end;

                end;
            */

        }

        private void AnalyseraSemsterUttag(int i, TPersoner Personer, int ID, string Variabel, decimal AntalDgr, string Art_dec, decimal SocavgProcent, string Period, double Manadslon)
        {
            /*
            procedure TFLonefaktura.AnalyseraSemsterUttag(var i: Integer;
      var Personer: TPersoner; ID: Integer; Variabel: String; AntalDgr: Double;
      Art_dec: String; SocavgProcent: Double; Period: String;Manadslon: Double);
    const
      delimiter = #9;
    var
      qu, quSem: TADOQuery;
      semers: Double;
      //semdgr: Double;
      Pers: TPersoner;
      Belopp,Belopptmp: String;
      SumBelopp : Double;
      tmp1, tmp2: String;
    begin
      qu:=nil;  //2013-01-18  Måste initieras annars ges en warning
      quSem:=nil;  //2013-01-18  Måste initieras annars ges en warning
      semers:=0;  //2013-01-18  Måste initieras annars ges en warning
      SumBelopp:= 0;
      try
        qu:= TADOQuery.Create(nil);
        qu.Connection := ADOConnection1;
        with qu do begin
          SQL.Add('select l.BELOPP from BA_VAR_LONART v, BA_LONPERTRANS l,BA_LONPER_PERS LONETRANS ');
          SQL.Add(' where v.variabel_txt = '+chr(39)+'grundlön_belopp'+chr(39));
          SQL.Add(' and l.lop_nr_loneart = v.lop_nr_loneart');
          SQL.Add(' and lonetrans.lop_nr_per_person = l.lop_nr_per_person');
          SQL.Add(' and lonetrans.p_id = '+ IntToStr(Personer.P_ID));
          SQL.Add(' and lonetrans.period_id = '+chr(39)+Period+chr(39));
          Open;
          if not eof then begin
            while not eof do begin
              SumBelopp:= SumBelopp + FieldByName('BELOPP').AsFloat;
              Next;
            end;
    //        // 2013-01-18 Artdec 1 ska inte längre göra påslag. FIltreras därefter på totalbeloppet så ingen post skapas
    //        if Art_dec = '1' then begin
    //          semers:= 0.04557;
    //          //semdgr:= 0.0267;  //2013-01-18  Används inte
    //        end
            if Art_dec = '2' then begin
              semers:= 0.054;
              //semdgr:= 0.0225;  //2013-01-18  Används inte
            end;
            if SumBelopp <> 0 then begin
              Pers.FTG_NR:= Personer.FTG_NR;
              Pers.P_ID:= Personer.P_ID;
              Pers.Namn:= Personer.Namn;
              Pers.ANST_NR:= Personer.ANST_NR;
              Pers.LoneartID:= ID;
              Pers.Benamning:= Variabel;
              Pers.Antal:= AntalDgr;
              if ID = 1004 then begin
                tmp1:= FormatFloat('0.00',AntalDgr * semers * Manadslon);
                if tmp1.StartsWith('-') <> true then begin
                  tmp2:= '-'+tmp1;
                end
                else begin
                  tmp2:= tmp1;
                end;
                Pers.Belopp:= StrToFloat(tmp2);
              end else
                Pers.Belopp:= StrToFloat(FormatFloat('0.00',AntalDgr * semers * Manadslon));//månadslön  SumBelopp
              Belopptmp := FloatToStr(Pers.Belopp);
              Belopp:= StringReplace(Belopptmp,'.',',',[rfReplaceAll]);
              Pers.Utbdag:= Personer.Utbdag;
              Pers.FakturaNr:= Personer.FakturaNr;
              Pers.Statuskod:= Personer.Statuskod;
              Pers.KarNr:= Personer.KarNr;
              Pers.KarNamn:= Personer.KarNamn;
              Pers.Kst:= Personer.Kst;
              Pers.Antal:= 1;
              if Pers.Belopp <> 0 then begin
                m_fil.Add(IntToStr(Pers.FTG_NR)+delimiter+Pers.Namn+delimiter+IntToStr(Pers.ANST_NR)+delimiter+IntToStr(Pers.LoneartID)+delimiter+Pers.Benamning+delimiter+'1'{FloatToStr(Pers.Antal)}+delimiter+Belopp+delimiter+Pers.Utbdag+delimiter+Pers.FakturaNr+delimiter+'0'+delimiter+IntToStr(Pers.KarNr)+delimiter+Pers.KarNamn+delimiter+'0'+delimiter+Pers.Kst);
                inc(m_Counter);
                SetLength(vvList,m_Counter);
                inc(i);
                vvList[i]:= Pers;
    //            // 2013-01-18  Inget påslag längre, bara grundsociala avgifter
    //            try
    //              quSem:= TADOQuery.Create(nil);
    //              quSem.Connection := ADOConnection1;
    //              with quSem do begin
    //                SQL.Add('SELECT VARDE FROM BA_VARIABEL WHERE VARIABEL_TXT = '+chr(39)+'FA_avtförs_'+Art_dec+chr(39));
    //                Open;
    //                if not eof then begin
    //                  SocavgProcent:= SocavgProcent + FieldByName('VARDE').AsFloat;
    //                end;
    //                Close;
    //              end;
    //            finally
    //              quSem.Free;
    //              quSem:= nil;
    //            end;
                AnalyseraSocialaAvgifterSemester(i,Pers, 1005, 'Sociala avgifter avdr. semester', SocavgProcent);
              end;
            end;

          end;
          Close;
        end;
      finally
        qu.Free;
        qu:= nil;
      end;

    end;

            */
        }

        private void AnalyseraSocialaAvgifterSemester(ref int i, ref TPersoner Personer, int ID, string Socavg, double Procent)
        {
            /*
                        procedure TFLonefaktura.AnalyseraSocialaAvgifterSemester(var i: Integer;
                  var Personer: TPersoner; ID: Integer; Socavg: String;
                  Procent: Double);
                const
                  delimiter = #9;
                var
                  //semers, semdgr: Double;  //2013-01-18  Används aldrig
                  Pers: TPersoner;
                  Belopp,Belopptmp: String;
                  //SumBelopp : Double;  //2013-01-18 Används aldrig
                  Proctmp: String;
                  //Procenten: String;  //2013-01-18 Används aldrig
                begin
                  Pers.FTG_NR:= Personer.FTG_NR;
                  Pers.P_ID:= Personer.P_ID;
                  Pers.Namn:= Personer.Namn;
                  Pers.ANST_NR:= Personer.ANST_NR;
                  Pers.LoneartID:= ID;
                  Pers.Benamning:= Socavg;
                  Pers.Antal:= 1;
                  Pers.Belopp:= StrToFloat(FormatFloat('0.00',Personer.Belopp * (Procent/100)));
                  Belopptmp := FloatToStr(Pers.Belopp);
                  Belopp:= StringReplace(Belopptmp,'.',',',[rfReplaceAll]);
                  Pers.Utbdag:= Personer.Utbdag;
                  Pers.FakturaNr:= Personer.FakturaNr;
                  Pers.Statuskod:= Personer.Statuskod;
                  Pers.KarNr:= Personer.KarNr;
                  Pers.KarNamn:= Personer.KarNamn;

                  Proctmp := FLoatToStr(Procent);
                  //Procenten:= StringReplace(Proctmp,',','.',[rfReplaceAll]);
                  Pers.Procent:= Procent;
                  Pers.Kst:= Personer.Kst;

                  m_fil.Add(IntToStr(Pers.FTG_NR)+delimiter+Pers.Namn+delimiter+IntToStr(Pers.ANST_NR)+delimiter+IntToStr(Pers.LoneartID)+delimiter+Pers.Benamning+delimiter+FloatToStr(Pers.Antal)+delimiter+Belopp+delimiter+Pers.Utbdag+delimiter+Pers.FakturaNr+delimiter+'0'+delimiter+IntToStr(Pers.KarNr)+delimiter+Pers.KarNamn+delimiter+Proctmp+delimiter+Pers.Kst);
                  inc(m_Counter);
                  SetLength(vvList,m_Counter);
                  inc(i);
                  vvList[i]:= Pers;
                end;

             */
        }

        private void AnalyseraSocialaAvgifterForman(ref int i, ref TPersoner Personer, int lop_nr_loneart, int ID, string Socavg, double Procent)
        {
            /*
            procedure TFLonefaktura.AnalyseraSocialaAvgifterForman(var i: Integer;
      var Personer: TPersoner; lop_nr_loneart, ID: Integer; Socavg: String;
      Procent: Double);
    const delimiter = #9;
    var
      //j: Integer;
      qu: TADOQuery;
      Pers: TPersoner;
      Proctmp: String;
      //Procenten: String;
      Belopp,Belopptmp: String;
    begin
      qu:=nil;  //2013-01-18  Måste initieras annars ges en warning
      try
        //j:=i;
        qu:= TADOQuery.Create(nil);
        qu.Connection := ADOConnection1;
        with qu do begin
          SQL.Add('SELECT * FROM BA_LONART WHERE LOP_NR_LONEART = '+IntToStr(lop_nr_loneart));
          Open;
          if not eof then begin
            if FieldByName('USOC').AsString = 'J' then begin //underlag sociala avg. loneartreg.
              Pers.FTG_NR:= Personer.FTG_NR;
              Pers.P_ID:= Personer.P_ID;
              Pers.Namn:= Personer.Namn;
              Pers.ANST_NR:= Personer.ANST_NR;
              Pers.LoneartID:= ID;
              Pers.Benamning:= Socavg;
              Pers.Antal:= 1;
              Pers.Belopp:= StrToFloat(FormatFloat('0.00',Personer.Belopp * (Procent/100)));//Summan av de sociala avg. på löenarten
              Belopptmp := FloatToStr(Pers.Belopp);
              Belopp:= StringReplace(Belopptmp,'.',',',[rfReplaceAll]);
              Pers.Utbdag:= Personer.Utbdag;
              Pers.FakturaNr:= Personer.FakturaNr;
              Pers.Statuskod:= Personer.Statuskod;
              Pers.KarNr:= Personer.KarNr;
              Pers.KarNamn:= Personer.KarNamn;

              Pers.Procent:= Procent;
              Pers.Kst:= Personer.Kst;
              Proctmp := FLoatToStr(Procent);
              //Procenten:= StringReplace(Proctmp,',','.',[rfReplaceAll]);

              m_fil.Add(IntToStr(Pers.FTG_NR)+delimiter+Pers.Namn+delimiter+IntToStr(Pers.ANST_NR)+delimiter+IntToStr(Pers.LoneartID)+delimiter+Pers.Benamning+delimiter+FloatToStr(Pers.Antal)+delimiter+Belopp+delimiter+Pers.Utbdag+delimiter+Pers.FakturaNr+delimiter+'0'+delimiter+IntToStr(Pers.KarNr)+delimiter+Pers.KarNamn+delimiter+Proctmp+delimiter+Pers.Kst);
              inc(m_Counter);
              SetLength(vvList,m_Counter);
              //inc(i);
              vvList[i]:= Pers;
            end;
          end;
          Close;
        end;
      finally
        qu.Free;
        qu:= nil;
      end;

    end;

            */
        }

        private string HamtaKst(int P_id)
        {
            /*
            function TFLonefaktura.HamtaKst(P_id: Integer): String;
    var
      qu: TADOQuery;
    begin
      qu:=nil;  //2013-01-18  Måste initieras annars ges en warning
      try
        qu:= TADOQuery.Create(nil);
        qu.Connection := ADOConnection1;
        with qu do begin
    //      SQL.Add('select  distinct(varde) kst');
    //      SQL.Add('from ba_ftg_pers bfp');
    //      SQL.Add('join alwar.ba_person bp on bp.p_id = '+IntToStr(P_id));
    //      SQL.Add('left outer join alwar.ba_pers_kontrub bpk on bpk.p_id = '+IntToStr(P_id)+' and bpk.ftg_nr = bfp.ftg_nr');
    //      SQL.Add('left outer join alwar.ba_kontbgr bk on bk.lop_nr_kontbgr = bpk.lop_nr_kontbgr and bk.lop_nr_kontrub in (');
    //      SQL.Add('Select lop_nr_kontrub');
    //      SQL.Add('from alwar.ba_kontrub');
    //      SQL.Add('where kort_namn = '+chr(39)+'kst'+chr(39)+')');

          SQL.Clear;
          //ny tabellstruktur from version 2007.2 av Agda PS
          SQL.Add('select  distinct(varde) kst');
          SQL.Add('from ba_ftg_pers bfp');
          SQL.Add('join alwar.ba_person bp on (bp.p_id = bfp.p_id)');
          SQL.Add('LEFT OUTER join BA_ANSTALLD_GRUND AG ON (bp.p_id = AG.P_ID and AG.ftg_nr = bfp.ftg_nr)');
          SQL.Add('LEFT OUTER join BA_ANSTALLD_KONTERING AK ON (AG.LOP_NR_ANSTALLD = AK.LOP_NR_ANSTALLD)');
          SQL.Add('left outer join ba_pers_kontrub bpk on (AK.LOP_NR_ANSTALLD_KONTERING = BPK.LOP_NR_ANSTALLD_KONTERING)');
          SQL.Add('left outer join ba_kontbgr bk on (bk.lop_nr_kontbgr = bpk.lop_nr_kontbgr ');
          SQL.Add('and bk.lop_nr_kontrub in (Select lop_nr_kontrub from alwar.ba_kontrub where kort_namn=' +chr(39)+'kst'+chr(39)+'))');
          SQL.Add('WHERE BFP.p_id =' + IntToStr(P_ID));
          SQL.Add(' AND ((AK.H_FOMTID IS NULL AND AK.H_TILLTID IS NULL)');
          SQL.Add(' OR (AK.H_FOMTID IS NULL AND AK.H_TILLTID > GETDATE())');
          SQL.Add('OR (AK.H_FOMTID <= GETDATE() AND AK.H_TILLTID IS NULL)');
          SQL.Add('OR (AK.H_FOMTID <= GETDATE() AND AK.H_TILLTID > GETDATE()))');
          SQL.Add('and bk.lop_nr_kontrub in (Select lop_nr_kontrub from alwar.ba_kontrub where kort_namn='+chr(39)+'kst'+chr(39)+')');

          Open;
          if not eof then begin
            while not eof do begin
              if FieldByName('KST').AsString <> '' then begin
                Result:= FieldByName('KST').AsString;
                Break;
              end;
              Next;
            end;
          end;
        end;
     finally
      qu.Free;
      qu:= nil;
     end;


    end;
            */
            return string.Empty;
        }

        private double PlockaUtSocialaAVG(int Pid, string Utbdag)
        {
            /*
            function TFLonefaktura.PlockaUtSocialaAVG(Pid: Integer;Utbdag:String): Double;
    var
      qu: TADOQuery;
      ars: String;
      ar,alder: Integer;
      Value: double;
    begin
      Value:=0;  //2013-01-18  Måste initieras annars ges en Warning
      qu:= TADOQuery.Create(nil);
      qu.Connection:= m_qu.Connection;
      with qu do begin
        SQL.Clear;
        SQL.Add('SELECT * FROM BA_PERSON WHERE P_ID = '+IntToStr(Pid));
        Open;
        if not eof then begin
          ars:= Copy(FieldByName('PERSONNR').AsString,1,4);
        end;
        ar:= StrToInt(ars);
        alder:= StrToInt(Copy(Utbdag,1,4)) - ar;
        if (ar < 1938) then begin //född före 1938
          SQL.Clear;
          SQL.Add('SELECT TOP 1 * FROM BA_SOCIALA WHERE SOCAVG_ID=''N''');
          Open;
          if not eof then
            Value:= FieldByName('SOCAVG_PROCENT').AsFloat;
        end else if (alder <= 26) then begin //26 eller yngre
          SQL.Clear;
          SQL.Add('SELECT TOP 1 * FROM BA_SOCIALA WHERE SOCAVG_ID=''57''');
          Open;
          if not eof then
            Value:= FieldByName('SOCAVG_PROCENT').AsFloat;
        end else if (ar >= 1938) and (alder >65) then begin//födda 1938 eller senare och äldre än 65
          SQL.Clear;
          SQL.Add('SELECT TOP 1 * FROM BA_SOCIALA WHERE SOCAVG_ID=''59''');
          Open;
          if not eof then
            Value:= FieldByName('SOCAVG_PROCENT').AsFloat;
        end else begin //full arbgivaravgift
          SQL.Clear;
          SQL.Add('SELECT TOP 1 * FROM BA_SOCIALA WHERE SOCAVG_ID=''55''');
          Open;
          if not eof then
            Value:= FieldByName('SOCAVG_PROCENT').AsFloat;
        end;
      end;


      qu.Free;
      qu:= nil;

      Result:= Value;

    end;
            */
            return 0;
        }
    }
}

