﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fakturering
{
    public partial class formInvoice : Form
    {
        public formInvoice()
        {
            InitializeComponent();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Fakturanummer kan med fördel läggas upp EN gång/år. Detta nummer (max 6 siffror)");
            sb.Append("kommer att ökas med ett för varje ny faktura som genereras från Agda PS");
            lblInvoiceNbr.Text = sb.ToString();
            lblFilePath.Text = "Sökväg till fil på max 50 tecken. Exempelvis c:\\Fakturafil";


            List<Company> comp = new List<Company>();

            using (SqlConnection connection = new SqlConnection(Helper.ConnectionString()))
            {
                connection.Open();

                StringBuilder cmd = new StringBuilder();

                cmd.AppendLine("SELECT FTG_NR,FORETAGSNAMN FROM BA_FTG WHERE FTG_NR = 3");

                SqlCommand command = new SqlCommand(cmd.ToString(), connection);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDataRecord record = reader;
                    Company c = new Company()
                    {
                        CompanyId = record.GetInt16(0),
                        Name = record.GetString(1)
                    };
                    comp.Add(c);
                }

                cbCompany.DataSource = comp;
                cbCompany.DisplayMember = "Name";
                cbCompany_SelectedIndexChanged(null, null);
            }
        }

        private void buSave_Click(object sender, EventArgs e)
        {
            /*
            procedure TFFaktura.buSparaClick(Sender: TObject);
var
  qu: TADOQuery;
begin
  qu:=nil;  //2013-01-18  Måste initieras annars ges en warning
  if (edSokvag.Text <> '') and (cbForetag.ItemIndex <> -1) then begin
     try
     if Length(edFakturanummer.Text) < 7 then begin
        qu:= TADOQuery.Create(nil);
        qu.Connection := m_qu.Connection;
      try
        with qu do begin
          SQL.Clear;
          SQL.Add('SELECT * FROM BA_FTG_INST WHERE FTG_NR= 3 AND INSTALLNING = '+chr(39)+'EGEN_FRALS_SOKVAG'+chr(39));
          Open;
          if not eof then begin
            Close;
            SQL.Clear;
            SQL.Add('UPDATE BA_FTG_INST SET VARDE = '+chr(39)+trim(edFakturanummer.Text)+chr(39)+' ,BESKRIVNING = '+chr(39)+trim(edSokvag.Text)+chr(39)+' WHERE FTG_NR= 3 AND INSTALLNING = '+chr(39)+'EGEN_FRALS_SOKVAG'+chr(39));
//            SQL.SaveToFile('C:\Sokvag.txt');
            ExecSQL;
          end else begin
            Close;
            SQL.Clear;
            SQL.Add('INSERT INTO BA_FTG_INST(FTG_NR,INSTALLNING,VARDE,BESKRIVNING,PARAMETERNAMN)');
            SQL.Add(' VALUES(3,'+chr(39)+'EGEN_FRALS_SOKVAG'+chr(39)+','+chr(39)+trim(edFakturanummer.Text)+chr(39)+','+chr(39)+trim(edSokvag.Text)+chr(39)+','+chr(39)+'EGEN_FRALS_SOKVAG'+chr(39)+')');
            ExecSQL;
          end;
          Close;
        end;
      except
        on e:exception do Showmessage(e.Message);
      end;
      end else begin
        Showmessage('Fakturanumret är begränsat till 6 siffror!');
      end;
     finally
      qu.Free;
      qu:= nil;
     end;
  end else begin
    if (cbForetag.ItemIndex = -1) then
      Showmessage('Inget företag är angivet!')
    else
      Showmessage('Ingen sökväg till filen är angiven!');
  end;
end;
            */
        }

        private void buClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buCancel_Click(object sender, EventArgs e)
        {
            //edFakturanummer.Text:= '';
        }

        private void buFilePath_Click(object sender, EventArgs e)
        {
            //if opSokvag.Execute then begin
            //edSokvag.Text := opSokvag.FileName;
            //end;
        }

        private void formInvoice_FormClosing(object sender, FormClosingEventArgs e)
        {
            //ForetagsID.Free;
            //m_qu.Free;
            //m_qu:= nil;
        }

        private void cbCompany_SelectedIndexChanged(object sender, EventArgs e)
        {

            using (SqlConnection connection = new SqlConnection(Helper.ConnectionString()))
            {
                connection.Open();

                StringBuilder cmd = new StringBuilder();

                cmd.AppendLine("SELECT TOP(1) VARDE, BESKRIVNING FROM BA_FTG_INST WHERE FTG_NR= 3 AND INSTALLNING = 'EGEN_FRALS_SOKVAG'");

                SqlCommand command = new SqlCommand(cmd.ToString(), connection);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDataRecord record = reader;
                    tbInvoiceID.Text = record.GetString(0);
                    tbFilePath.Text = record.GetString(1);
                }
            }
        }
    }

}
